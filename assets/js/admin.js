jQuery(document).ready(function($) {

    var swmpdf_color_picker = $( '.swmpdf-color-picker' );

    if ( swmpdf_color_picker.length ) {
        swmpdf_color_picker.wpColorPicker();
    }

    var custom_uploader;
    $('.upload_media_button').click(function(e) {
        e.preventDefault();
        //If the uploader object has already been created, reopen the dialog
        if (custom_uploader) {
            custom_uploader.open();
            return;
        }
        //Extend the wp.media object
        custom_uploader = wp.media.frames.file_frame = wp.media({
            title: 'Choose Image',
            button: {
                text: 'Choose Image'
            },
            multiple: false,
            library: {
                type: 'image'
            },
        });

        var media_field = $(this).siblings('.swmpdf-media');
        var media_button = $(this);

        //When a file is selected, grab the URL and set it as the text field's value
        custom_uploader.on('select', function() {
            attachment = custom_uploader.state().get('selection').first().toJSON();
            media_field.val(attachment.url);
            if(media_button.siblings('img')) {
                media_button.siblings('img').attr('src', attachment.url);
            }
            else {
                media_button.after('<img src="' + attachment.url + '" style="width:140px; height:140px; margin-top:20px; display:block;">')
            }
        });
        //Open the uploader dialog
        custom_uploader.open();
    });
});
