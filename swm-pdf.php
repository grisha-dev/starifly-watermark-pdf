<?php
/*
 * Plugin Name: Starifly Watermark PDF
 * Description: Watermarks PDF attachments on Unit pages (WPLMS theme)
 * Version: 1.0
 * Author: Starifly
 */
defined( 'ABSPATH' ) || exit;

if ( ! class_exists( 'SWMPDF' ) ) {

    class SWMPDF
    {

        public $version = '1.0.1';
        private static $instance;

        /**
         * Init
         */
        public static function init()
        {
            if ( null === self::$instance ) {
                self::$instance = new self();
            }
            return self::$instance;
        }

        /**
         * Constructor
         */
        public function __construct()
        {

            define('SWMPDF_BASE_FILE', plugin_basename(__FILE__));
            define('SWMPDF_BASE_URL', trailingslashit( plugins_url( 'swm-pdf' ) ) );
            define('SWMPDF_PATH', plugin_dir_path(__FILE__));
            define('SWMPDF_VERSION', $this->version);

            $this->includes();
            $this->load_hooks();

        }

        /**
         * Load required classes
         */
        private function includes()
        {
            require_once(SWMPDF_PATH . 'classes/class-options.php');
            require_once(SWMPDF_PATH . 'classes/class-swmpdf-watermark.php');
            require_once(SWMPDF_PATH . 'classes/class-swmpdf-file-handler.php');
        }

        /**
         * Remove woocommerce_download_product action hook and replace
         */
        public function load_hooks()
        {
            if (is_admin()) {
                add_filter('plugin_action_links_' . SWMPDF_BASE_FILE, array($this, 'add_settings_link'));
            }

            add_action('admin_menu', array($this, 'add_settings_menu_item'));
            add_action('admin_enqueue_scripts', array($this, 'load_admin_scripts'));

            add_filter('wp_get_attachment_link', array($this,'swmpdf_link'), 10, 2);

        }

        public function swmpdf_link($link, $id)
        {
            $post = get_post($id);

            if ('application/pdf' === $post->post_mime_type && (get_post_type() === 'unit' || get_page_template_slug()==='start.php')) {
                $url = add_query_arg('pdf_id', $id, site_url());
                $post_title = isset($post->post_title) ? $post->post_title : '';
                return '<a href="' . esc_url($url) . '" class="swmpdf-link">' . $post_title . '</a>';
            }

            return $link;
        }

        public function load_admin_scripts() {
            if ( isset($_GET['page']) && $_GET['page'] === 'watermark-pdf' ) {
                wp_enqueue_media();
                wp_enqueue_style( 'wp-color-picker' );
                wp_enqueue_script( 'wp-color-picker-alpha', SWMPDF_BASE_URL . 'assets/js/libs/wp-color-picker-alpha.js', array( 'wp-color-picker' ), SWMPDF_VERSION, true );
                wp_enqueue_script('swmpdf-scripts', SWMPDF_BASE_URL . 'assets/js/admin.js', array( 'jquery', 'wp-color-picker-alpha' ), SWMPDF_VERSION, true);
            }
        }

        /**
         * Add settings link to plugin page
         */
        public function add_settings_link($links)
        {

            $settings = sprintf('<a href="%s" title="%s">%s</a>', admin_url('admin.php?page=watermark-pdf'), __('Go to the settings page'), __('Settings'));
            array_unshift($links, $settings);
            return $links;

        }

        /**
         * Add settings menu item
         */

        public function add_settings_menu_item()
        {
            add_menu_page(__("Watermark PDF"), __("Watermark PDF"), "manage_options", "watermark-pdf", array($this, "settings_page"), null, 99);
        }

        /**
         * Callback used to render the admin settings page.
         *
         */
        public function settings_page()
        {

            ?>
            <div class="wrap">
                <h1>Settings</h1>
                <form method="post" action="options.php">
                    <?php

                    settings_fields("swmpdf-settings");
                    do_settings_sections("watermark_pdf_options");
                    submit_button();
                    ?>
                </form>
            </div>
            <?php
        }


    }
}
add_action( 'plugins_loaded', array( 'SWMPDF', 'init' ), 10 );