<?php

defined( 'ABSPATH' ) || exit;

if ( ! class_exists( 'SWMPDF_File_Handler' ) ) {

    class SWMPDF_File_Handler {

        /**
         * Constructor
         */
        public function __construct() {
            add_action('init', array($this, 'pdf_file_download_path'));
        }
    
        public function pdf_file_download_path(  ) {

            if (isset($_GET['pdf_id']) && !empty($_GET['pdf_id'])) {

                add_filter('trp_stop_translating_page', '__return_true');

                $footer_input = $this->swmpdf_setup_watermark();
                $init_file = get_attached_file($_GET['pdf_id']);
                $swmpdf_file_path = str_replace( '.pdf', '', $init_file ) . '_' . time() . '.pdf';

                try {
                    SWMPDFWatermark::apply_and_spit( $init_file, $swmpdf_file_path, $footer_input );
                } catch ( Exception $ex ) {
                    _e( 'Unable to watermark this file for download. Please notify site administrator.');
                    echo $ex->getMessage();
                    return;
                };
            }
        }
    
        /**
         * Returns file_path, either altered/stamped or not
         */
        public static function swmpdf_setup_watermark() {

            $footer_input = '';

            if(!empty(SWMPDF_Options::get('user_id')) && is_user_logged_in()) {
                $user = wp_get_current_user();
                $footer_input .= $user->display_name . ' ';
            }
            if(!empty($date_format = SWMPDF_Options::get('date_format'))) {
                $footer_input .= date($date_format);
            }

            $out_charset = 'UTF-8';
            $footer_input = html_entity_decode( $footer_input, ENT_QUOTES | ENT_XML1, $out_charset );
            $footer_input = self::toASCII( $footer_input );

            return $footer_input;
     
        }
        
        public static function toASCII( $str ) {
            return strtr(utf8_decode($str),
            utf8_decode(
                'ŠŒŽšœžŸ¥µÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝßàáâãäåæçèéêëìíîïðñòóôõöøùúûüýÿ'),
                'SOZsozYYuAAAAAAACEEEEIIIIDNOOOOOOUUUUYsaaaaaaaceeeeiiiionoooooouuuuyy');
        }
    }

    new SWMPDF_File_Handler();
    
}