<?php

defined( 'ABSPATH' ) || exit;

if ( ! class_exists( 'SWMPDFWatermark' ) ) :

	class SWMPDFWatermark {

		public function __construct($origfile, $newfile, $footer) {

			$this->pdf = new setasign\Fpdi\Woo_Fpdi();
			$this->file = $origfile;
			$this->newfile = $newfile;
			$this->footer = $footer; 

		}

		public static function apply_and_spit($origfile, $newfile, $footer) {

			// FPDF Copyright 2011-2015 Olivier PLATHEY
			require_once SWMPDF_PATH . 'inc/fpdf/fpdf-rotate.php';
			// FPDI Copyright 2004-2018 Setasign - Jan Slabon
            require_once( SWMPDF_PATH . 'inc/fpdi/autoload.php' );

			$wm = new SWMPDFWatermark($origfile, $newfile, $footer);

            $wm->do_watermark();
            return $wm->newfile;
		
		}

		public function do_watermark() {

			$pagecount = $this->pdf->setSourceFile( $this->file );

			$text_x = SWMPDF_Options::get('text_x');
			$image_x = SWMPDF_Options::get('image_x');
			$watermark_y = SWMPDF_Options::get('watermark_y');

			$font = SWMPDF_Options::get( 'text_font', 'helvetica' );
			$text_size = SWMPDF_Options::get('text_font_size');
			$this->pdf->SetFont( $font, '', $text_size );
			$text_color = $this->parseRGBA( SWMPDF_Options::get('text_color') );
			$this->pdf->SetTextColor( $text_color['r'],$text_color['g'],$text_color['b'] );
			$text_angle = SWMPDF_Options::get('text_angle');

			$image_url = SWMPDF_Options::get('image_url');
			$image_w = SWMPDF_Options::get('image_w');
			$image_h = SWMPDF_Options::get('image_h');
			$image_angle = SWMPDF_Options::get('image_angle');
			$image_opacity = SWMPDF_Options::get('image_opacity') ?: '1';


			for( $i = 1; $i <= $pagecount; $i++ ) {
                $tplidx = $this->pdf->importPage($i);
                $specs = $this->pdf->getTemplateSize($tplidx);
                $this->pdf->addPage( $specs['height'] > $specs['width'] ? 'P' : 'L');

                $this->pdf->SetAlpha(1);
                $this->pdf->useTemplate( $tplidx, null, null, $specs['width'], $specs['height'], true );

                if($this->footer) {
                    $this->pdf->SetAlpha($text_color['a']);
                    $this->RotatedText($text_x, $watermark_y, $this->footer, $text_angle);
                }
                if($image_url) {
                    $this->pdf->SetAlpha($image_opacity);
                    $this->RotatedImage($image_url, $image_x, $watermark_y, $image_w, $image_h, $image_angle);
                }

			}

			$this->pdf->Output( 'D', basename($this->newfile), true);

			exit();


		} // end function do_watermark

		protected function hex2rgb($hex) {
			$hex = str_replace("#", "", $hex);
			$r = hexdec(substr($hex,0,2));
			$g = hexdec(substr($hex,2,2));
			$b = hexdec(substr($hex,4,2));
			$rgb = array($r, $g, $b);
			return implode(",", $rgb);
		}

        /**
         * Parses an RGBA string and returns and array with each color
         * @param  string $str The RGBA string, ex: "rgba(66, 66, 66, 1)"
         * @return array       Returns an array with each r, g, b and a value or false if an invalid string is passed in.
         */
        function parseRGBA($str){
            //match the rgba string and get it's part
            if(preg_match('/rgba\( *([\d\.-]+), *([\d\.-]+), *([\d\.-]+), *([\d\.-]+) *\)/i', $str, $m)){
                $out = array(
                    'r'=>intval($m[1]), //get the red
                    'g'=>intval($m[2]), //get the green
                    'b'=>intval($m[3]), //get the blue
                    'a'=>floatval($m[4]), //get the alpha and scale to 0-255
                );

                //clamp each  value between 0 and 255
                array_walk($out, function(&$v){ $v = min(255, max(0, $v)); });

                return $out;
            }
            return false;
        }

        public function RotatedText($x, $y, $txt, $angle)
        {
            //Text rotated around its origin
            $this->pdf->Rotate($angle,$x,$y);
            $this->pdf->Text($x,$y,$txt);
            $this->pdf->Rotate(0);
        }

        function RotatedImage($file,$x,$y,$w,$h,$angle)
        {
            //Image rotated around its upper-left corner
            $this->pdf->Rotate($angle,$x,$y);
            $this->pdf->Image($file,$x,$y,$w,$h);
            $this->pdf->Rotate(0);
        }

	} // end Class SWMPDFWatermark
	
endif;