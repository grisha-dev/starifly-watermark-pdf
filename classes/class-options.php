<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

if ( ! class_exists( 'SWMPDF_Options' ) ) {

    /**
     * Class ezTOC_Option
     *
     * Credit: Adapted from Easy Digital Downloads.
     */
    final class SWMPDF_Options
    {

        /**
         * Register the plugins core settings and options.
         *
         * @access private
         * @since  1.0
         * @static
         */
        public static function register()
        {

            if ( FALSE === get_option( 'swmpdf-settings' ) ) {

                add_option( 'swmpdf-settings', self::getDefaults() );
            }

            add_settings_section(
                'swmpdf_section',
                __return_null(),
                '__return_false',
                'watermark_pdf_options'
            );

            foreach (self::getRegistered() as $option) {


                    $name = isset($option['name']) ? $option['name'] : '';

                    add_settings_field(
                        'swmpdf-settings[' . $option['id'] . ']',
                        $name,
                        method_exists(__CLASS__, $option['type']) ? array(__CLASS__, $option['type']) : array(__CLASS__, 'missingCallback'),
                        'watermark_pdf_options',
                        'swmpdf_section',
                        array(
                            'id'          => isset($option['id']) ? $option['id'] : NULL,
                            'desc'        => !empty($option['desc']) ? $option['desc'] : '',
                            'name'        => isset($option['name']) ? $option['name'] : NULL,
                            'size'        => isset($option['size']) ? $option['size'] : NULL,
                            'options'     => isset($option['options']) ? $option['options'] : '',
                            'default'     => isset($option['default']) ? $option['default'] : '',
                            'min'         => isset($option['min']) ? $option['min'] : NULL,
                            'max'         => isset($option['max']) ? $option['max'] : NULL,
                            'step'        => isset($option['step']) ? $option['step'] : NULL,
                            'chosen'      => isset($option['chosen']) ? $option['chosen'] : NULL,
                            'placeholder' => isset($option['placeholder']) ? $option['placeholder'] : NULL,
                            'allow_blank' => isset($option['allow_blank']) ? $option['allow_blank'] : TRUE,
                            'readonly'    => isset($option['readonly']) ? $option['readonly'] : FALSE,
                            'faux'        => isset($option['faux']) ? $option['faux'] : FALSE,
                        )
                    );


            }
            register_setting('swmpdf-settings', 'swmpdf-settings');

        }



        private  static  function getRegistered() {
            $options = array(
                'user_id' => array(
                    'id' => 'user_id',
                    'name' => __( 'User ID'),
                    'type' => 'checkbox'
                ),
                'date_format' => array(
                    'id' => 'date_format',
                    'name' => __( 'Date format'),
                    'desc' => _('Enter valid date format or leave empty to not add a date'),
                    'type' => 'text'
                ),
                'text_font_size' => array(
                    'id' => 'text_font_size',
                    'name' => __( 'Text font size'),
                    'type' => 'number',
                ),
                'text_color' => array(
                    'id' => 'text_color',
                    'name' => __( 'Text color'),
                    'type' => 'color',
                ),
                'text_angle' => array(
                    'id' => 'text_angle',
                    'name' => __( 'Text angle'),
                    'type' => 'number',
                ),
                'image_url' => array(
                    'id' => 'image_url',
                    'name' => __( 'Image'),
                    'type' => 'media',
                ),
                'image_w' => array(
                    'id' => 'image_w',
                    'name' => __( 'Image width'),
                    'type' => 'number',
                ),
                'image_h' => array(
                    'id' => 'image_h',
                    'name' => __( 'Image height'),
                    'type' => 'number',
                ),
                'image_angle' => array(
                    'id' => 'image_angle',
                    'name' => __( 'Image angle'),
                    'type' => 'number',
                ),
                'image_opacity' => array(
                    'id' => 'image_opacity',
                    'name' => __( 'Image opacity'),
                    'type' => 'number',
                    'step' => 0.01
                ),
                'text_x' => array(
                    'id' => 'text_x',
                    'name' => __( 'Text offset X'),
                    'type' => 'number',
                ),
                'image_x' => array(
                    'id' => 'image_x',
                    'name' => __( 'Image offset X'),
                    'type' => 'number',
                ),
                'watermark_y' => array(
                    'id' => 'watermark_y',
                    'name' => __( 'Offset Y'),
                    'type' => 'number',
                ),
            );

            return  $options;
        }

        public static function text( $args, $value = NULL ) {

            if ( is_null( $value ) ) {

                $value = self::get( $args['id'], $args['default'] );
            }

            $id = 'swmpdf-settings[' . $args['id'] . ']';

            $readonly = isset( $args['readonly'] ) && $args['readonly'] === TRUE ? ' readonly="readonly"' : '';
            $size     = isset( $args['size'] ) && ! is_null( $args['size'] ) ? $args['size'] : 'regular';

            $html = '<input type="text" class="' . $size . '-text" id="' . $id . '" ' . 'name="' . $id . '" ' . 'value="' . esc_attr( stripslashes( $value ) ) . '"' . $readonly . '/>';

            if ( 0 < strlen( $args['desc'] ) ) {

                $html .= '<label for="swmpdf-settings[' . $args['id'] . ']"> ' . $args['desc'] . '</label>';
            }

            echo $html;
        }

        public static function number( $args ) {

            $value = self::get( $args['id'], $args['default'] );

            $name = 'name="swmpdf-settings[' . $args['id'] . ']"';

            $readonly = isset( $args['readonly'] ) && $args['readonly'] === TRUE ? ' readonly="readonly"' : '';
            $size     = isset( $args['size'] ) && ! is_null( $args['size'] ) ? $args['size'] : 'regular';
            $step     = isset( $args['step'] ) && !empty( $args['step'] ) ? ' step="' . $args['step'] . '"' : '';

            $html = '<input type="number" class="' . $size . '-text" id="swmpdf-settings[' . $args['id'] . ']"' . $name . ' value="' . esc_attr( stripslashes( $value ) ) . '"' . $readonly . $step . ' style="width:60px"/>';

            if ( 0 < strlen( $args['desc'] ) ) {

                $html .= '<label for="swmpdf-settings[' . $args['id'] . ']"> ' . $args['desc'] . '</label>';
            }

            echo $html;
        }

        public static function checkbox( $args, $value = NULL ) {

            if ( is_null( $value ) ) {

                $value = self::get( $args['id'], $args['default'] );
            }

            $name = 'name="swmpdf-settings[' . $args['id'] . ']"';

            $checked = $value ? checked( 1, $value, FALSE ) : '';

            $html = '<input type="checkbox" id="swmpdf-settings[' . $args['id'] . ']"' . $name . ' value="1" ' . $checked . '/>';

            if ( 0 < strlen( $args['desc'] ) ) {

                $html .= '<label for="swmpdf-settings[' . $args['id'] . ']"> ' . $args['desc'] . '</label>';
            }

            echo $html;
        }

        public static function color( $args ) {

            $value = self::get( $args['id'], $args['default'] );

            $default = isset( $args['default'] ) ? $args['default'] : '';

            $html  = '<input type="text" class="swmpdf-color-picker color-picker" id="swmpdf-settings[' . $args['id'] . ']" name="swmpdf-settings[' . $args['id'] . ']" value="' . esc_attr( $value ) . '" data-default-color="' . esc_attr( $default ) . '" data-alpha="true"/>';

            if ( 0 < strlen( $args['desc'] ) ) {

                echo '<label for="swmpdf-settings[' . $args['id'] . ']"> ' . $args['desc'] . '</label>';
            }

            echo $html;
        }

        public static function media ($args) {

            $value = self::get( $args['id'], $args['default'] );

            $size     = isset( $args['size'] ) && ! is_null( $args['size'] ) ? $args['size'] : 'regular';

            $html  = '<input type="text" class="swmpdf-media ' . $size . '-text" id="swmpdf-settings[' . $args['id'] . ']" name="swmpdf-settings[' . $args['id'] . ']" value="' . esc_attr( $value ) . '" />';
            $html .= '<input class="upload_media_button button" type="button" value="Upload" />';

            if(!empty($value)) {
                $html .= '<img src="' . $value . '" style="width:140px; height:140px; margin-top:20px; display:block;">';
            }

            if ( 0 < strlen( $args['desc'] ) ) {

                echo '<label for="swmpdf-settings[' . $args['id'] . ']"> ' . $args['desc'] . '</label>';
            }

            echo $html;
        }

        public static function missingCallback( $args ) {

            printf(
                __( 'The callback function used for the <strong>%s</strong> setting is missing.'),
                $args['id']
            );
        }

        private static function getDefaults() {

            $defaults = array(
                'user_id' => '',
                'date_format' => '',
                'text_font_size' => 14,
                'text_color' => 'rgba(0,0,0,1)',
                'text_angle' => '',
                'image_url' => '',
                'image_w' => 200,
                'image_h' => 200,
                'image_angle' => '',
                'image_opacity' => 1,
                'text_x' => 47,
                'image_x' => 60,
                'watermark_y' => 260,
            );

            return  $defaults;
        }

        /**
         * Get the default options array.
         */
        private static function getOptions() {

            $defaults = self::getDefaults();
            $options  = get_option( 'swmpdf-settings', $defaults );

            return  $options;
        }

        /**
         * Get option value by key name.
         */
        public static function get( $key, $default = FALSE ) {

            $options = self::getOptions();

            return array_key_exists( $key, $options ) ? $options[ $key ] : $default;
        }
    }



    add_action( 'admin_init', array( 'SWMPDF_Options', 'register' ) );
}